import config from '../config.json'
import {authHeader} from "./auth-header";
import {history} from '../services/history';



function getAll()
{
  const requestOptions =
  {
    method: 'GET',
    headers: authHeader(true)
  };

  return fetch(`${config.API_BASE_URL}/cars`, requestOptions).then(handleResponse);
}


function create(obj)
{
  const requestOptions =
  {
    method: 'POST',
    headers: authHeader(true),
    body: JSON.stringify(obj)
  };

  return fetch(`${config.API_BASE_URL}/cars`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function handleResponse(response)
{
  return new Promise((resolve, reject) =>
  {
    if (!response.ok)
    {
      if (response.status === 401)
      {
        // auto logout if 401 response returned from api
        history.push('/login');
        history.go();
      }

      const error = new Error(response.statusText);
      error.status = response.status;

      return reject(error);
    }
    else
    {
      return resolve(response.json());
    }
  });
}

export const carService =
{
  getAll,
  create
};
