import config from '../config.json';
import {history} from '../services/history';
import { authHeader } from './auth-header';

function getOne(id)
{
  const requestOptions =
    {
      method: 'GET',
      headers: authHeader()
    };

  return fetch(`${config.API_BASE_URL}/users/${id}`, requestOptions).then(handleResponse);
}

function create(obj)
{
  const requestOptions =
  {
    method: 'POST',
    headers: authHeader(true),//{ 'Content-Type': 'application/json' },
    body: JSON.stringify(obj)
  };

  return fetch(`${config.API_BASE_URL}/users`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function edit(id, obj)
{
  const requestOptions =
  {
    method: 'PUT',
    headers: authHeader(true), //{ 'Content-Type': 'application/json' },
    body: JSON.stringify(obj)
  };

  return fetch(`${config.API_BASE_URL}/users/${id}`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function remove(id)
{
  const requestOptions =
  {
    method: 'DELETE',
    headers: authHeader(true)//{ 'Content-Type': 'application/json' }
  };

  return fetch(`${config.API_BASE_URL}/users/${id}`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}


function login(username, password)
{
  const requestOptions =
  {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username, password })
  };

  return fetch(`${config.API_BASE_URL}/users/login?api_key=${config.API_KEY}`, requestOptions)
  .then((response) =>
  {
    return response.json();
  })
  .then(data =>
  {

    if (data.token)
    {
      // store user details and jwt token in local storage to keep user logged in between page refreshes
      localStorage.setItem('user', JSON.stringify(data));
      localStorage.setItem('token', data.token);
      localStorage.setItem('username', data.user.username);
      localStorage.setItem("user_id", data.user._id);
    }

    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function logout()
{
  // remove user from local storage to log user out
  localStorage.removeItem('user');
}

function getAll()
{
  const requestOptions =
  {
    method: 'GET',
    headers: authHeader()
  };

  return fetch(`${config.API_BASE_URL}/users`, requestOptions).then(handleResponse);
}


function handleResponse(response)
{
  return new Promise((resolve, reject) =>
  {
    if (!response.ok)
    {
      if (response.status === 401)
      {
        // auto logout if 401 response returned from api
        logout();
        history.push('/login');
        history.go();
      }

      const error = new Error(response.statusText);
      error.status = response.status;

      return reject(error);
    }
    else
    {
      return resolve(response.json());
    }
  });
}

export const userService =
{
  login,
  logout,
  getAll,
  getOne,
  create,
  edit,
  remove
};
