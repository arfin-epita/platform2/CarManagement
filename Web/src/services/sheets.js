import config from '../config.json'
import {authHeader} from "./auth-header";
import {history} from '../services/history';



function getAll()
{
  const requestOptions =
    {
      method: 'GET',
      headers: authHeader(true)
    };

  return fetch(`${config.API_BASE_URL}/sheets`, requestOptions).then(handleResponse);
}


function getOne(id)
{
  const requestOptions =
    {
      method: 'GET',
      headers: authHeader(true)
    };

  return fetch(`${config.API_BASE_URL}/sheets/${id}`, requestOptions).then(handleResponse);
}

function create(obj)
{
  const requestOptions =
    {
      method: 'POST',
      headers: authHeader(true),
      body: JSON.stringify(obj)
    };

  return fetch(`${config.API_BASE_URL}/sheets`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function edit(id, obj)
{
  const requestOptions =
    {
      method: 'PUT',
      headers: authHeader(true),
      body: JSON.stringify(obj)
    };

  return fetch(`${config.API_BASE_URL}/sheets/${id}`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function remove(id)
{
  const requestOptions =
    {
      method: 'DELETE',
      headers: authHeader(true)
    };

  return fetch(`${config.API_BASE_URL}/sheets/${id}`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function createActivity(sheet_id, obj)
{
  const requestOptions =
  {
    method: 'POST',
    headers: authHeader(true),
    body: JSON.stringify(obj)
  };

  return fetch(`${config.API_BASE_URL}/sheets/${sheet_id}/activities`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}

function removeActivity(sheet_id, activity_id)
{
  const requestOptions =
  {
    method: 'DELETE',
    headers: authHeader(true)
  };

  return fetch(`${config.API_BASE_URL}/sheets/${sheet_id}/activities/${activity_id}`, requestOptions)
  .then(handleResponse)
  .then(data =>
  {
    return data;
  })
  .catch(err =>
  {
    console.error(err);
  })
}


function handleResponse(response)
{
  return new Promise((resolve, reject) =>
  {
    if (!response.ok)
    {
      if (response.status === 401)
      {
        // auto logout if 401 response returned from api
        history.push('/login');
        history.go();
      }

      const error = new Error(response.statusText);
      error.status = response.status;

      return reject(error);
    }
    else
    {
      return resolve(response.json());
    }
  });
}

export const sheetService =
{
  getAll,
  getOne,
  create,
  edit,
  remove,
  createActivity,
  removeActivity
};
