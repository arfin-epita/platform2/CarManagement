export function authHeader(isJSON = false)
{
  // return authorization header with jwt token

  if (localStorage.getItem("token") && localStorage.getItem("token"))
  {
    const obj = {'Authorization': 'Bearer ' + localStorage.getItem("token")};
    if (isJSON)
      obj['Content-Type'] = "application/json";

    return obj
  }
  else
  {
    return {};
  }
}