import {constants} from '../constants/index'

let user = JSON.parse(localStorage.getItem('user'));

const initialState = user ? { loggedIn: true, user } : {};

function authentication(state = initialState, action)
{
  switch (action.type)
  {
    case constants.USERS.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case constants.USERS.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case constants.USERS.LOGIN_FAILURE:
      return {};
    case constants.USERS.LOGOUT:
      return {};
    default:
      return state
  }
}

function users(state = {}, action)
{
  const newState = Object.assign({}, state);

  switch (action.type)
  {
    case constants.USERS.REQUEST:
    {
      newState['loading'] = true;
      break;
    }
    case constants.USERS.ERROR:
    {
      newState['error'] = action.error;
      break;
    }
    case constants.USERS.GETALL_SUCCESS:
    {
      newState['items'] = action.users;
      break;
    }
    case constants.USERS.GETONE_SUCCESS:
    {
      newState['item'] = action.user;
      break;
    }
    case constants.USERS.CREATE_SUCCESS:
    {
      newState['items'] = [...newState.items, action.user];
      break;
    }
    case constants.USERS.EDIT_SUCCESS:
    {
      newState['item'] = action.user;
      for (let i = 0; i < newState.items.length; i++)
      {
        if (newState.items[i]._id === action.user._id)
        {
          newState.items[i] = action.user;
          break;
        }
      }
      break;
    }
    case constants.USERS.REMOVE_SUCCESS:
    {
      newState['items'] = newState.items.filter(el => el._id !== action.user._id);
      newState['item'] = null;
      break;
    }
    default:
      return state
  }

  return newState;
}
export {authentication, users};