import {constants} from '../constants/index'

function main(state = {}, action)
{
  const newState = Object.assign({}, state);

  switch (action.type)
  {
    case constants.MAIN.IS_ADDING:
    {
      newState['isAdding'] = action.val;
      return newState;
    }
    case constants.MAIN.IS_EDITING:
    {
      newState['isEditing'] = action.val;
      return newState;
    }
    default:
    {
      return newState;
    }

  }
}

export {main};