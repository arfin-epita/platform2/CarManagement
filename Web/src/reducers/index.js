import {combineReducers} from 'redux';

import {users, authentication} from './users';
import {sheets} from "./sheets";
import {main} from "./main";

const rootReducer = combineReducers(
{
  users,
  main,
  sheets,
  authentication
});

export default rootReducer;