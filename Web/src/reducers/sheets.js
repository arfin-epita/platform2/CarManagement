import {constants} from '../constants/index'


function sheets(state = {}, action)
{
  const newState = Object.assign({}, state);

  switch (action.type)
  {
    case constants.SHEETS.REQUEST:
    {
      newState['loading'] = true;
      break;
    }
    case constants.SHEETS.ERROR:
    {
      newState['error'] = action.error;
      break;
    }
    case constants.SHEETS.GETALL_SUCCESS:
    {
      newState['items'] = action.sheets;
      break;
    }
    case constants.SHEETS.GETALL_CARS_SUCCESS:
    {
      newState['cars'] = action.cars;
      break;
    }
    case constants.SHEETS.GETONE_SUCCESS:
    {
      newState['item'] = action.sheet;
      break;
    }
    case constants.SHEETS.CREATE_SUCCESS:
    {
      newState['items'] = [...newState.items, action.sheet];
      break;
    }
    case constants.SHEETS.CREATE_CAR_SUCCESS:
    {
      newState['cars'] = [...newState.cars, action.car];
      break;
    }
    case constants.SHEETS.CREATE_ACTIVITY_SUCCESS:
    {
      newState['item'] = action.sheet;
      break;
    }
    case constants.SHEETS.REMOVE_ACTIVITY_SUCCESS:
    {
      newState['item'] =  action.sheet;
      //newState['item']['work']['activities'] = newState.item.work.activities.filter(el => el._id !== action.activity_id);
      //return newState;
      break;
    }
    case constants.SHEETS.EDIT_SUCCESS:
    {
      newState['item'] = action.sheet;

      for (let i = 0; i < newState.items.length; i++)
      {
        if (newState.items[i]._id === action.sheet._id)
        {
          newState.items[i] = action.sheet;
          break;
        }
      }

      break;
    }
    case constants.SHEETS.REMOVE_SUCCESS:
    {
      newState['items'] = newState.items.filter(el => el._id !== action.sheet._id);
      newState['item'] = null;
      break;
    }
    default:
      return state
  }

  return newState;
}

export {sheets};