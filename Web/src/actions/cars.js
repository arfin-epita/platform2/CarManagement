import { constants } from '../constants';
import { carService } from '../services/cars';
import {history} from "../services/history";


function getAll()
{

  return dispatch =>
  {
    dispatch(request());

    carService
    .getAll()
    .then(
    cars => dispatch(success(cars.items)),
    error => dispatch(failure(error))
    );
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(cars) {return {type: constants.SHEETS.GETALL_CARS_SUCCESS, cars}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}


function create(obj)
{
  return dispatch =>
  {
    dispatch(request());

    carService
    .create(obj)
    .then((data) =>
    {
      dispatch(success(data.item));
    })
    .catch(err =>
    {
      dispatch(failure(err));
    });
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(car) {return {type: constants.SHEETS.CREATE_CAR_SUCCESS, car}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}



export const carActions =
{
  getAll,
  create
};
