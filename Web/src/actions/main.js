import {constants} from "../constants";

function setIsAdding(val)
{
  return {type: constants.MAIN.IS_ADDING, val};
}

function setIsEditing(val)
{
  return {type: constants.MAIN.IS_EDITING, val}
}

export const mainActions =
{
  setIsAdding,
  setIsEditing
};