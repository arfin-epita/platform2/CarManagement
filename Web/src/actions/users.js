import { constants } from '../constants';
import { userService } from '../services/users';
import {history} from "../services/history";


function login(username, password)
{
  return dispatch =>
  {
    dispatch(request({ username }));

    userService
    .login(username, password)
    .then(user =>
    {
        dispatch(success(user));
        //history.push("/dashboard");
    },
    error =>
    {
        dispatch(failure(error));
    });
  };

  function request(user) { return { type: constants.USERS.LOGIN_REQUEST, user } }
  function success(user) { return { type: constants.USERS.LOGIN_SUCCESS, user } }
  function failure(error) { return { type: constants.USERS.LOGIN_FAILURE, error } }
}

function logout() {
  userService.logout();
  return { type: constants.USERS.LOGOUT };
}

function getAll()
{

  return dispatch =>
  {
    dispatch(request());

    userService.getAll()
    .then(
      users => dispatch(success(users.items)),
      error => dispatch(failure(error))
    );
  };

  function request() {return {type: constants.USERS.REQUEST}}
  function success(users) {return {type: constants.USERS.GETALL_SUCCESS, users}}
  function failure(error) {return {type: constants.USERS.ERROR, error}}
}

function getOne(id)
{
  return dispatch => {
    dispatch(request());

    userService.getOne(id).then(
      data => dispatch(success(data.item)),
      error => dispatch(failure(error))
    );
  };

  function request() {return {type: constants.USERS.REQUEST}}
  function success(user) {return {type: constants.USERS.GETONE_SUCCESS, user}}
  function failure(error) {return {type: constants.USERS.ERROR, error}}
}

function create(obj)
{
  console.log(obj);
  return dispatch => {
    dispatch(request());

    userService
    .create(obj)
    .then((data) =>
    {
      dispatch(success(data));
      if (data)
      {
        history.push("/users/" + data._id);
        history.go();
      }
    })
    .catch(err =>
    {
      dispatch(failure(err));
    });
  };

  function request() {return {type: constants.USERS.REQUEST}}
  function success(user) {return {type: constants.USERS.CREATE_SUCCESS, user}}
  function failure(error) {return {type: constants.USERS.ERROR, error}}
}


function edit(id, obj)
{
  return dispatch =>
  {
    dispatch(request());

    userService
    .edit(id, obj)
    .then((data) =>
    {
      dispatch(success(data.item));
      if (data.item)
      {
        history.push("/users/" + data.item._id);
        history.go();
      }
    })
    .catch(err =>
    {
      dispatch(failure(err));
    });
  };

  function request() {return {type: constants.USERS.REQUEST}}
  function success(user) {return {type: constants.USERS.EDIT_SUCCESS, user}}
  function failure(error) {return {type: constants.USERS.ERROR, error}}
}

function remove(id)
{
  return dispatch => {
    dispatch(request());

    userService
    .remove(id)
    .then((data) =>
    {
      dispatch(success(data));
      history.push("/users");
      history.go();
    })
    .catch(error =>
    {
      dispatch(failure(error));
    });
  };

  function request() {return {type: constants.USERS.REQUEST}}
  function success(user) {return {type: constants.USERS.REMOVE_SUCCESS, user}}
  function failure(error) {return {type: constants.USERS.ERROR, error}}
}


export const userActions =
{
  login,
  logout,
  getAll,
  getOne,
  create,
  edit,
  remove
};
