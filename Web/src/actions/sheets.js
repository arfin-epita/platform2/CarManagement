import { constants } from '../constants';
import {sheetService} from "../services/sheets";
import {history} from "../services/history";
import {mainActions} from "./main";


function getAll()
{
  return dispatch =>
  {
    dispatch(request());

    sheetService
    .getAll()
    .then(
      sheets => dispatch(success(sheets.items)),
      error => dispatch(failure(error))
    );
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(sheets) {return {type: constants.SHEETS.GETALL_SUCCESS, sheets}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}

function getOne(id)
{
  return dispatch => {
    dispatch(request());

    sheetService
    .getOne(id)
    .then((data) =>
    {
      dispatch(success(data.item));
    })
    .catch(error =>
    {
      dispatch(failure(error))
    });
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(sheet) {return {type: constants.SHEETS.GETONE_SUCCESS, sheet}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}

function create(obj)
{
  return dispatch => {
    dispatch(request());

    sheetService
    .create(obj)
    .then((data) =>
    {
      dispatch(success(data.item));
      if (data.item)
      {
        history.push("/sheets/" + data.item._id);
        history.go();
      }
    })
    .catch(error =>
    {
      dispatch(failure(error));
    });
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(sheet) {return {type: constants.SHEETS.CREATE_SUCCESS, sheet}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}


function edit(id, obj)
{
  return dispatch =>
  {
    dispatch(request());

    sheetService
    .edit(id, obj)
    .then((data) =>
    {
      dispatch(success(data.item));
      dispatch(mainActions.setIsEditing(false));
    })
    .catch(error =>
    {
      dispatch(failure(error))
    });
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(sheet) {return {type: constants.SHEETS.EDIT_SUCCESS, sheet}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}

function remove(id)
{
  return dispatch =>
  {
    dispatch(request());

    sheetService
    .remove(id)
    .then((data) =>
    {
      dispatch(success(data));
      history.push("/sheets");
      history.go();
    })
    .catch(error =>
    {
      dispatch(failure(error));
    });
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(sheet) {return {type: constants.SHEETS.REMOVE_SUCCESS, sheet}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}

function createActivity(sheet_id, obj)
{
  return dispatch =>
  {
    dispatch(request());

    sheetService
    .createActivity(sheet_id, obj)
    .then((data) =>
    {
      dispatch(success(data.item));
      if (data.item)
      {
        history.go();
      }
    })
    .catch(error =>
    {
      dispatch(failure(error));
    });
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(sheet) {return {type: constants.SHEETS.CREATE_ACTIVITY_SUCCESS, sheet}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}

function removeActivity(sheet_id, activity_id)
{
  return dispatch =>
  {
    dispatch(request());

    sheetService
    .removeActivity(sheet_id, activity_id)
    .then((data) =>
    {
      dispatch(success(data.item, activity_id));
    })
    .catch(error =>
    {
      dispatch(failure(error));
    });
  };

  function request() {return {type: constants.SHEETS.REQUEST}}
  function success(sheet, activity_id) {return {type: constants.SHEETS.REMOVE_ACTIVITY_SUCCESS, sheet, activity_id}}
  function failure(error) {return {type: constants.SHEETS.ERROR, error}}
}

export const sheetActions =
{
  getAll,
  getOne,
  create,
  edit,
  remove,
  createActivity,
  removeActivity
};
