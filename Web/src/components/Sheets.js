import React from 'react';
import { connect } from 'react-redux';
import * as Element from 'element-react';
import {sheetActions} from "../actions/sheets";
import {carActions} from "../actions/cars";
import {mainActions} from "../actions/main";
import {history} from "../services/history";

class Sheets extends React.Component
{
  constructor(props)
  {
    super(props);

    this.state =
    {
      newItem:
      {
        car_id: "",
        title: "",
        user_id: localStorage.getItem("user_id"),
        car_mileage: "",
        date_begin: null,
        date_end: null,
        work: {}
      },
      newCar:
      {
        brand: "",
        model: "",
        year: "",
        numberplate: ""
      },
      isAddingCar: false
    };
  }

  componentWillMount()
  {
    this.props.getAll();
    this.props.getAllCars();
  }

  static displayItem(item)
  {
    history.push("/sheets/"+item._id);
    history.go();
  }

  setNewItem(prop, value)
  {
    const newItem = this.state.newItem;
    newItem[prop] = value;

    this.setState({newItem: newItem});
  }

  setNewCar(prop, value)
  {
    const newCar = this.state.newCar;
    newCar[prop] = value;

    this.setState({newCar: newCar});
  }

  createCarFunc()
  {
    this.props.createCar(this.state.newCar);
    this.setState({isAddingCar: false});
    this.setState(
    {
      newCar:
      {
        brand: "",
        model: "",
        year: "",
        numberplate: ""
      }
    });
  }

  render()
  {
    if (this.props.isAdding)
    {
      return (
        <div className="mt-2">
          <Element.Form className="en-US" model={this.state.newItem} labelWidth="120" >

            <Element.Form.Item label="Titre">
              <Element.Input value={this.state.newItem.title} onChange={(txt) => this.setNewItem("title", txt)}/>
            </Element.Form.Item>

            <Element.Form.Item label="Véhicule">
              <Element.Select value={this.state.newItem.car_id} placeholder="Merci de sélectionner un véhicule" style={{width: "100%"}} onChange={(txt) => this.setNewItem("car_id", txt)}>
                {
                  this.props.cars &&
                  this.props.cars.map(el =>
                  {
                    return <Element.Select.Option key={el._id} label={el.brand + " "+ el.model+ " " + el.year+" - "+el.numberplate} value={el._id} />
                  })
                }
              </Element.Select>
            </Element.Form.Item>

            <Element.Form.Item label="Kilométrage du véhicule">
              <Element.Input type="number" value={this.state.newItem.car_mileage} onChange={(txt) => this.setNewItem("car_mileage", txt)}/>
            </Element.Form.Item>

            <Element.Form.Item label="Date de début">
              <Element.DatePicker
                value={this.state.newItem.date_begin}
                placeholder="Choisir un jour"
                style={{width: "100%"}}
                onChange={(date) => this.setNewItem("date_begin", date)}/>
            </Element.Form.Item>

            <Element.Form.Item label="Date de fin">
              <Element.DatePicker
              value={this.state.newItem.date_end}
              placeholder="Choisir un jour"
              onChange={(date) => this.setNewItem("date_end", date)}
              style={{width: "100%"}} />
            </Element.Form.Item>

            <Element.Form.Item>
              <Element.Button type="primary" onClick={(e) => this.props.create(this.state.newItem)}>Créer</Element.Button>
              <Element.Button type="danger" onClick={(e) => this.props.toggleAdd(false)}>Annuler</Element.Button>
            </Element.Form.Item>
          </Element.Form>
        </div>
      )
    }
    else {
      return (
        <div>
          <div className="w-100">
            <h1 className="mt-2">Fiches</h1>
            <button type="button" className="btn btn-primary" onClick={() => this.props.toggleAdd(true)}>Ajouter une fiche</button>
            <button type="button" className="btn btn-secondary ml-2" onClick={() => this.setState({isAddingCar : true})}>Ajouter un véhicule</button>

            <table className="table table-hover mt-2">
              <thead>
              <tr>
                <th scope="col">Titre</th>
                <th scope="col">Immatriculation du véhicule</th>
                <th scope="col">Véhicule</th>
                <th scope="col">Date de début</th>
                <th scope="col">Date de fin</th>
              </tr>
              </thead>
              <tbody>
              {
                this.props.items &&

                this.props.items.map((item, index) => {
                  return (
                    <tr key={index} onClick={(e) => Sheets.displayItem(item)}>
                      <td>{item.title}</td>
                      <td>{item.car_id.numberplate}</td>
                      <td>{item.car_id.brand} - {item.car_id.model} - {item.car_id.year}</td>
                      <td>{item.date_begin}</td>
                      <td>{item.date_end}</td>
                    </tr>
                  )
                })
              }
              </tbody>
            </table>
          </div>

          <Element.Dialog
            title="Ajouter un nouveau véhicule"
            size="normal"
            visible={ this.state.isAddingCar}
            onCancel={ () => this.setState({ isAddingCar: false }) }
            lockScroll={ false }>

            <Element.Dialog.Body>
              <Element.Form className="en-US" model={this.state.newCar} labelWidth="120" >

                <Element.Form.Item label="Marque">
                  <Element.Input value={this.state.newCar.brand} onChange={(txt) => this.setNewCar("brand", txt)}/>
                </Element.Form.Item>

                <Element.Form.Item label="Modèle">
                  <Element.Input value={this.state.newCar.model} onChange={(txt) => this.setNewCar("model", txt)}/>
                </Element.Form.Item>

                <Element.Form.Item label="Année">
                  <Element.Input value={this.state.newCar.year} onChange={(txt) => this.setNewCar("year", txt)}/>
                </Element.Form.Item>

                <Element.Form.Item label="Immatriculation">
                  <Element.Input value={this.state.newCar.numberplate} onChange={(txt) => this.setNewCar("numberplate", txt)}/>
                </Element.Form.Item>

              </Element.Form>
            </Element.Dialog.Body>
            <Element.Dialog.Footer className="dialog-footer">
              <Element.Button onClick={ () => this.setState({ isAddingCar: false }) }>Annuler</Element.Button>
              <Element.Button type="primary" onClick={ () => this.createCarFunc()}>Ajouter</Element.Button>
            </Element.Dialog.Footer>
          </Element.Dialog>
        </div>
      );

    }
  }
}

const mapStateToProps = (state, ownProps) =>
{
  return {
    items: state.sheets.items,
    cars: state.sheets.cars,
    isAdding: state.main.isAdding
  };
};

const mapDispatchToProps = (dispatch) =>
{
  return {
    getAll: () => dispatch(sheetActions.getAll()),
    getAllCars: () => dispatch(carActions.getAll()),
    create: (obj) => dispatch(sheetActions.create(obj)),
    createCar: (obj) => dispatch(carActions.create(obj)),
    toggleAdd: (val) => dispatch(mainActions.setIsAdding(val))
  }
};

const connectedSheets = connect(mapStateToProps, mapDispatchToProps)(Sheets);

export {connectedSheets as SheetsComponent};