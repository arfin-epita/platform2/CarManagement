import { Link } from 'react-router-dom'
import React from "react";

import * as Element from 'element-react'

class SideBar extends React.Component
{

  constructor(props)
  {
    super(props);
  }

  render()
  {
    if (localStorage.getItem("user")) {
      return (
        <div className="wrapper h-100">
          <nav id="sidebar" className="sidebar-nav h-100">
            <ul className="list-unstyled components">
              <li className="active">
                <Link to="/sheets">Fiches</Link>
              </li>
              <li>
                <Link to="/users">Utilisateurs</Link>
              </li>
            </ul>
          </nav>
        </div>);
    }
    else
      return "";
  }


}

export {SideBar as SideBarComponent};