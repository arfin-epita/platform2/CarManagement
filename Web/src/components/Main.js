import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import * as Element from 'element-react';

import {PrivateRoute} from "./PrivateRoute";
import {TopBarComponent} from './TopBar'
import {SideBarComponent} from "./SideBar";
import {SheetsComponent} from "./Sheets";
import {UsersComponent} from "./Users";
import {LoginComponent} from "./Login";
import {UserComponent} from "./User";
import {SheetComponent} from "./Sheet";

class Main extends  React.Component
{

  render()
  {
    return (
      <div className="h-100">
        <TopBarComponent/>
        <div className="row container-fluid h-100">
          <div className="col-sm-3 ml-0 pl-0">
            <SideBarComponent/>
          </div>
          <div className="col-sm-9 h-100">

            <Switch>
              <Route exact path='/login' component={LoginComponent} />

              <PrivateRoute exact path='/' component={SheetsComponent}/>
              <PrivateRoute exact path='/sheets' component={SheetsComponent}/>
              <PrivateRoute path="/sheets/:id" component={SheetComponent}/>
              <PrivateRoute exact path='/users' component={UsersComponent}/>
              <PrivateRoute path="/users/:id" component={UserComponent}/>
            </Switch>

          </div>
        </div>
      </div>);
  }
}


export {Main as MainComponent};
