import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import '../static/css/App.css';
import {MainComponent} from "./Main";
import {LoginComponent} from "./Login";
import { i18n } from 'element-react'
import locale from 'element-react/src/locale/lang/fr'

i18n.use(locale);

class App extends Component
{
  render()
  {
    return (
      <div className="App h-100">
        <MainComponent/>
      </div>
    );
  }
}

export default App;
