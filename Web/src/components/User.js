import React from 'react';

import { connect } from 'react-redux';
import * as Element from 'element-react'
import {userActions} from '../actions/users';
import {mainActions} from '../actions/main'

class User extends React.Component
{

  constructor(props)
  {
   super(props);

  }

  componentWillMount()
  {
    this.props.getOne(this.props.match.params.id);
  }


  render()
  {

    if (this.props.isEditing)
    {
      return (
      <div className="mt-2">
        <Element.Form className="en-US" model={this.props.item} labelWidth="120" >

          <Element.Form.Item label="Nom">
            <Element.Input type="text" value={this.props.item.name} onChange={(txt) => this.props.item.name = txt}/>
          </Element.Form.Item>

          <Element.Form.Item label="Prénom">
            <Element.Input type="text" value={this.props.item.firstname} onChange={(txt) => this.props.item.firstname = txt}/>
          </Element.Form.Item>

          <Element.Form.Item label="Nom d'utilisateur">
            <Element.Input type="text" value={this.props.item.username} onChange={(txt) => this.props.item.username= txt}/>
          </Element.Form.Item>


          <Element.Form.Item label="Poste">
            <Element.Select
                value={this.props.item.position}
                placeholder="Merci de sélectionner un poste"
                style={{width: "100%"}}
                onChange={(txt) => this.props.item.position = txt}>
              <Element.Select.Option label="Mécanicien" value="Mécanicien"/>
              <Element.Select.Option label="Carrossier" value="Carrossier"/>
              <Element.Select.Option label="Chef de projet" value="Chef de projet"/>
            </Element.Select>
          </Element.Form.Item>

          <Element.Form.Item label="Rôle">
            <Element.Select
                value={this.props.item.role}
                placeholder="Merci de sélectionner un rôle"
                style={{width: "100%"}}
                onChange={(txt) => this.props.item.role = txt}>
              <Element.Select.Option label="Utilisateur" value="user"/>
              <Element.Select.Option label="Administrateur" value="admin"/>
            </Element.Select>
          </Element.Form.Item>

          <Element.Form.Item>
            <Element.Button type="warning" onClick={(e) => this.props.edit(this.props.item._id, this.props.item)}>Editer</Element.Button>
            <Element.Button type="danger" onClick={(e) => this.props.toggleEdit(false)}>Annuler</Element.Button>
          </Element.Form.Item>
        </Element.Form>
      </div>);

    }
    else
    {
      return (
        <div>

          <div className="btn-group mt-2" role="group">
            <button type="button" className="btn btn-warning" onClick={() => this.props.toggleEdit(true)}>Editer</button>
            <button type="button" className="btn btn-danger ml-2" onClick={() => this.props.remove(this.props.item._id)}>Supprimer</button>
          </div>

          {
            this.props.item &&
            <Element.Card
              className="box-card mt-3"
              header=
                {
                  <div className="clearfix">
                    <span style={{"lineHeight": "36px"}}>Informations</span>
                  </div>
                }>

              <div className="text item">Nom : {this.props.item.name}</div>
              <div className="text item">Prénom : {this.props.item.firstname}</div>
              <div className="text item">Nom d'utilisateur : {this.props.item.username}</div>
              <div className="text item">Rôle : {this.props.item.role}</div>
              <div className="text item">Poste : {this.props.item.position}</div>
            </Element.Card>
          }
          </div>
      )
    }
  }
}

const mapStateToProps = (state, ownProps) =>
{
  return {
    item: state.users.item,
    isEditing: state.main.isEditing};
};

const mapDispatchToProps = (dispatch) =>
{
  return {
    getOne: (id) => dispatch(userActions.getOne(id)),
    edit: (id, obj) => dispatch(userActions.edit(id, obj)),
    remove: (id) => dispatch(userActions.remove(id)),
    toggleEdit: (val) => dispatch(mainActions.setIsEditing(val))
  }
};

const connectedUser = connect(mapStateToProps, mapDispatchToProps)(User);

export {connectedUser as UserComponent};