import React from "react";
import { connect } from 'react-redux';
import * as Element from 'element-react'
import { userActions } from '../actions/users';
import {history} from '../services/history';

class TopBar extends React.Component
{
  constructor(props)
  {
    super(props);

    this.logout = this.logout.bind(this);
  }

  logout(e)
  {
    const { dispatch } = this.props;
    dispatch(userActions.logout());
    history.push("/login");
    history.go();
  }

  render()
  {
      if (localStorage.getItem("user"))
      {

        const profile_url = "/users/"+localStorage.getItem("user_id");

        return (
          <div>
            <nav className="navbar navbar-dark bg-dark">
              <a className="navbar-brand" href={"/sheets"}>Car Manager</a>

              <div className="my-2 my-lg-0">
                <a href={profile_url} className="text-white boldOneHover" style={{textDecoration: 'none'}}>
                  {localStorage.getItem("username")}
                </a>
                <a className="text-white ml-2 boldOneHover" style={{cursor:'pointer'}} onClick={() => this.logout()}>
                  Deconnexion
                </a>
              </div>
            </nav>
          </div>)
      }
      else
        return "";
  }
}


function mapStateToProps(state)
{
  return {};
}

const TopBarConnected = (connect(mapStateToProps)(TopBar));
export { TopBarConnected as TopBarComponent};

