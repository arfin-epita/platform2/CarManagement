import React from 'react';
import { connect } from 'react-redux';
import * as Element from 'element-react'
import {history} from "../services/history";
import {userActions} from '../actions/users';
import {mainActions} from "../actions/main";

class Users extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state =
    {
      name: "",
      firstname: "",
      username: "",
      password: "",
      position: "",
      role: ""
    };
  }

  componentWillMount()
  {
    this.props.getAll();
  }

  static displayItem(item)
  {
    history.push("/users/"+item._id);
    history.go();
  }

  onSubmitAddForm(e)
  {

    this.props.create(this.state);
  }

  render()
  {
    if (this.props.isAdding)
    {

      return (
        <div className="mt-2">
          <Element.Form className="en-US" model={this.state} labelWidth="120" >

            <Element.Form.Item label="Nom">
              <Element.Input type="text" value={this.state.name} onChange={(txt) => this.setState({name: txt})}/>
            </Element.Form.Item>

            <Element.Form.Item label="Prénom">
              <Element.Input type="text" value={this.state.firstname} onChange={(txt) => this.setState({firstname: txt})}/>
            </Element.Form.Item>

            <Element.Form.Item label="Nom d'utilisateur">
              <Element.Input type="text" value={this.state.username} onChange={(txt) => this.setState({username: txt})}/>
            </Element.Form.Item>

            <Element.Form.Item label="Mot de passe">
              <Element.Input type="password" value={this.state.password} onChange={(txt) => this.setState({password: txt})}/>
            </Element.Form.Item>

            <Element.Form.Item label="Poste">
              <Element.Select value={this.state.position} placeholder="Merci de sélectionner un poste" style={{width: "100%"}} onChange={(txt) => this.setState({position: txt})}>
                <Element.Select.Option label="Mécanicien" value="Mécanicien"/>
                <Element.Select.Option label="Carrossier" value="Carrossier"/>
                <Element.Select.Option label="Chef de projet" value="Chef de projet"/>
              </Element.Select>
            </Element.Form.Item>

            <Element.Form.Item label="Rôle">
              <Element.Select value={this.state.role} placeholder="Merci de sélectionner un rôle" style={{width: "100%"}} onChange={(txt) => this.setState({role: txt})}>
                <Element.Select.Option label="Utilisateur" value="user"/>
                <Element.Select.Option label="Administrateur" value="admin"/>
              </Element.Select>
            </Element.Form.Item>

            <Element.Form.Item>
              <Element.Button type="primary" onClick={(e) => this.onSubmitAddForm(e)}>Créer</Element.Button>
              <Element.Button type="danger" onClick={(e) => this.props.toggleAdd(false)}>Annuler</Element.Button>
            </Element.Form.Item>
          </Element.Form>
        </div>
      )
    }
    else {
      return (
        <div>
          <div className="w-100">
            <h1 className="mt-2">Utilisateurs</h1>
            <button type="button" className="btn btn-primary" onClick={() => this.props.toggleAdd(true)}>Ajouter</button>

            <table className="table table-hover mt-2">
              <thead>
              <tr>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Utilisateur</th>
                <th scope="col">Rôle</th>
                <th scope="col">Poste</th>
              </tr>
              </thead>
              <tbody>
              {
                this.props.items &&

                this.props.items.map((item, index) => {
                  return (
                    <tr key={index} onClick={(e) => Users.displayItem(item)}>
                      <td>{item.name}</td>
                      <td>{item.firstname}</td>
                      <td>{item.username}</td>
                      <td>{item.role}</td>
                      <td>{item.position}</td>
                    </tr>
                  )
                })
              }
              </tbody>
            </table>
          </div>
        </div>
      );
    }
  }


}


const mapStateToProps = (state, ownProps) =>
{
  return {
    items: state.users.items,
    isAdding: state.main.isAdding
  };
};

const mapDispatchToProps = (dispatch) =>
{
  return {
    getAll: () => dispatch(userActions.getAll()),
    toggleAdd: (val) => dispatch(mainActions.setIsAdding(val)),
    create: (obj) => dispatch(userActions.create(obj))
  }
};

const connectedUsers = connect(mapStateToProps, mapDispatchToProps)(Users);

export {connectedUsers as UsersComponent};