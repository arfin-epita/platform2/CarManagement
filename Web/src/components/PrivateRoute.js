import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'


export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem("user")
      ? <Component {...props} />
      : <Redirect to='/login' />
  )} />);