import React from 'react';
import {Link} from 'react-router-dom';

import { connect } from 'react-redux';
import * as Element from 'element-react'
import {sheetActions} from '../actions/sheets';
import {mainActions} from '../actions/main'
import {carActions} from "../actions/cars";

class Sheet extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state =
    {
      newActivity:
      {
        title: "",
        description: "",
        price: "",
        VAT: "",
        state: "init",
      },
      isAddingActivity: false
    };

  }

  setNewActivity(prop, value)
  {
    const newActivity = this.state.newActivity;
    newActivity[prop] = value;

    this.setState({newActivity: newActivity});
  }

  componentWillMount()
  {
    this.props.getOne(this.props.match.params.id);
  }

  createActivityFunc()
  {
    this.props.createActivity(this.props.item._id, this.state.newActivity);
    this.setState({isAddingActivity: false});
    this.setState(
    {
      newActivity:
      {
        title: "",
        description: "",
        price: "",
        VAT: "",
        state: "init",
      }
    });
  }



  render()
  {

    if (this.props.isEditing)
    {
      return (
      <div className="mt-2">
        <Element.Form className="en-US" model={this.props.item} labelWidth="120" >

          <Element.Form.Item label="Titre">
            <Element.Input value={this.props.item.title} onChange={(txt) => this.props.item.title = txt}/>
          </Element.Form.Item>


          <Element.Form.Item label="Kilométrage du véhicule">
            <Element.Input type="number" value={this.props.item.car_mileage} onChange={(txt) => this.props.item.car_mileage = txt}/>
          </Element.Form.Item>

          <Element.Form.Item label="Date de début">
            <Element.DatePicker
            value={new Date(this.props.item.date_begin)}
            placeholder="Choisir un jour"
            style={{width: "100%"}}
            onChange={(date) => this.props.item.date_begin = date}/>
          </Element.Form.Item>

          <Element.Form.Item label="Date de fin">
            <Element.DatePicker
            value={new Date(this.props.item.date_end)}
            placeholder="Choisir un jour"
            onChange={(date) => this.props.item.date_end  = date}
            style={{width: "100%"}} />
          </Element.Form.Item>

          <Element.Form.Item>
            <Element.Button type="warning" onClick={(e) => this.props.edit(this.props.item._id, this.props.item)}>Editer</Element.Button>
            <Element.Button type="danger" onClick={(e) => this.props.toggleEdit(false)}>Annuler</Element.Button>
          </Element.Form.Item>
        </Element.Form>
      </div>);

    }
    else
    {
      return (


      <div>

        <div className="btn-group mt-2" role="group">
          <button type="button" className="btn btn-warning" onClick={() => this.props.toggleEdit(true)}>Editer</button>
          <button type="button" className="btn btn-danger ml-2" onClick={() => this.props.remove(this.props.item._id)}>Supprimer</button>
        </div>

        {
          this.props.item && this.props.item.car_id && this.props.item.user_id &&

          <Element.Card
          className="box-card mt-3"
          header=
          {
            <div className="clearfix">
              <span style={{"lineHeight": "36px"}}>Informations</span>
            </div>
          }>

            <div className="text item"><strong>{this.props.item.title}</strong></div>
            <div className="text item">Véhicule : {this.props.item.car_id.brand} - {this.props.item.car_id.model} - {this.props.item.car_id.year}</div>
            <div className="text item">Immatriculation : {this.props.item.car_id.numberplate}</div>
            <div className="text item">Kilométrage du véhicule : {this.props.item.car_mileage}</div>
            <div className="text item">Ressource concernée: <Link to={"/users/"+this.props.item.user_id._id}> {this.props.item.user_id.firstname} {this.props.item.user_id.name}</Link></div>
            <div className="text item">Date de début d'intervention : {this.props.item.date_begin}</div>
            <div className="text item">Date de fin d'intervention : {this.props.item.date_end}</div>
          </Element.Card>
        }

        {
          this.props.item && this.props.item.work &&
          <Element.Card
          className="box-card mt-3"
          header=
          {
            <div className="clearfix">
              <span style={{"lineHeight": "36px"}}>Activités</span>
              <span className="col text-right">
                <Element.Button type="primary" onClick={(e) => this.setState({isAddingActivity: true})}>Ajouter</Element.Button>
              </span>
            </div>
          }>
            {
              this.props.item.work.activities.map((activity) =>
              {
                return (
                <div key={activity._id}>
                  <Element.Button type="danger" className="float-right"
                                  onClick={() => this.props.removeActivity(this.props.item._id, activity._id)}>Supprimer</Element.Button>
                  <div className="text item clearfix"><strong>{activity.title}</strong></div>
                  <div className="text item">Description : {activity.description}</div>
                  <div className="text item">Prix HT : {activity.price}</div>
                  <div className="text item">Pourcentage TVA : {activity.VAT}</div>
                  <div className="text item">Statut : {activity.state}</div>
                </div>);
              })
            }
          </Element.Card>
        }

        <Element.Dialog
        title="Ajouter une nouvelle activité"
        size="normal"
        visible={ this.state.isAddingActivity}
        onCancel={ () => this.setState({ isAddingActivity: false }) }
        lockScroll={ false }>

          <Element.Dialog.Body>
            <Element.Form className="en-US" model={this.state.newCar} labelWidth="120" >

              <Element.Form.Item label="Titre">
                <Element.Input value={this.state.newActivity.title} onChange={(txt) => this.setNewActivity("title", txt)}/>
              </Element.Form.Item>

              <Element.Form.Item label="Description">
                <Element.Input type="textarea" value={this.state.newActivity.description} onChange={(txt) => this.setNewActivity("description", txt)}/>
              </Element.Form.Item>

              <Element.Form.Item label="Prix">
                <Element.Input value={this.state.newActivity.price} onChange={(txt) => this.setNewActivity("price", txt)}/>
              </Element.Form.Item>

              <Element.Form.Item label="Pourcentage TVA">
                <Element.Input value={this.state.newActivity.VAT} onChange={(txt) => this.setNewActivity("VAT", txt)}/>
              </Element.Form.Item>

              <Element.Form.Item label="Statut">
                <Element.Select
                    value={this.state.newActivity.state}
                    style={{width: "100%"}}
                    onChange={(txt) => this.setNewActivity("state", txt)}>
                  <Element.Select.Option label="Init" value="init" />
                  <Element.Select.Option label="En cours" value="started" />
                  <Element.Select.Option label="Terminée" value="finished" />
                </Element.Select>
              </Element.Form.Item>

            </Element.Form>
          </Element.Dialog.Body>
          <Element.Dialog.Footer className="dialog-footer">
            <Element.Button onClick={ () => this.setState({ isAddingActivity: false }) }>Annuler</Element.Button>
            <Element.Button type="primary" onClick={ () => this.createActivityFunc()}>Ajouter</Element.Button>
          </Element.Dialog.Footer>
        </Element.Dialog>
      </div>);
    }
  }
}

const mapStateToProps = (state, ownProps) =>
{
  return {
    item: state.sheets.item,
    cars: state.sheets.cars,
    isEditing: state.main.isEditing};
};

const mapDispatchToProps = (dispatch) =>
{
  return {
    getOne: (id) => dispatch(sheetActions.getOne(id)),
    getAllCars: () => dispatch(carActions.getAll()),
    createActivity: (sheet_id, obj) => dispatch(sheetActions.createActivity(sheet_id, obj)),
    removeActivity: (sheet_id, activity_id) => dispatch(sheetActions.removeActivity(sheet_id, activity_id)),
    edit: (id, obj) => dispatch(sheetActions.edit(id, obj)),
    remove: (id) => dispatch(sheetActions.remove(id)),
    toggleEdit: (val) => dispatch(mainActions.setIsEditing(val))
  }
};

const connectedSheet = connect(mapStateToProps, mapDispatchToProps)(Sheet);

export {connectedSheet as SheetComponent};
