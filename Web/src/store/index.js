import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';

const loggerMiddleware = createLogger();

const initialStore =
{
  users: {items: [], item: {}},
  main: {isAdding: false, isEditing: false},
  sheets: {items: [], item: {}, cars: []}
};

export const store = createStore(
  rootReducer,
  initialStore,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
);