require('module-alias/register');

const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const config = require('config');

// Handlers
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');
const mongooseHandler = require('@mongooseHandler');
mongooseHandler.connect(config.get("MONGODB"));
const authHandler = require('@authHandler');



authHandler.configure(config.get('AUTH'));


const router = require('./router');

const app = express();


app.use(morgan('dev', {stream: logHandler.stream}));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(helmet());
app.use(cors());

app.use('/api', router);


// catch 404 and forward to error handler
app.use(function(req, res, next)
{
  next(errorHandler.create(404, "Not found"));
});

// error handler
app.use(function(err, req, res, next)
{
  return errorHandler.send(res, err);
});

module.exports = app;
