
const errorHandler = require('@errorHandler');

const modules =
{

  main:
  {
    routerMain: "@./router.js"
  },

  document:
  {
    user: "@models/User.js",
    activity: "@models/Activity.js",
    car: "@models/Car.js",
    customer: "@models/Customer.js",
    role: "@models/Role.js",
    sheet: "@models/Sheet.js"
  },

  subdocument:
  {
    activity: "@models/Activity.js"
  },

  plugin:
  {
    meta: "@./node_modules/module"
  },

  module:
  {
    users:
    {
      router: "@modules/users/router/index",
      lib: "@modules/users/lib/index",
      dataAccess: "@modules/users/dataAccess/index"
    },

    customers:
    {
      router: "@modules/customers/router/index",
      lib: "@modules/customers/lib/index",
      dataAccess: "@modules/customers/dataAccess/index"
    },

    sheets:
    {
      router: "@modules/sheets/router/index",
      lib: "@modules/sheets/lib/index",
      dataAccess: "@modules/sheets/dataAccess/index"
    },

    cars:
    {
      router: "@modules/cars/router/index",
      lib: "@modules/cars/lib/index",
      dataAccess: "@modules/cars/dataAccess/index"
    }
  }
};

/**
 *
 * @param {string} type
 * @param {string} module_
 * @param {string | null} layer
 * @return {*|null}
 */
function get(type, module_, layer = null)
{
  try
  {
    const path = (layer === null) ?
    modules[type][module_] :
    modules[type][module_][layer];

    return require(path);

  }
  catch (e)
  {
    errorHandler.log(e);
    return null;
  }
}

/**
 *
 * @param {string} type
 * @param {string} module_
 * @param {string|null} layer
 * @param {string} val
 * @return {boolean}
 */
function set(type, module_, layer = null, val)
{
  if (layer == null)
  {
    modules[type][module_] = val;
    return true;
  }

  modules[type][module_][layer] = val;
  return true;
}

module.exports =
{
  get,
  set
};
