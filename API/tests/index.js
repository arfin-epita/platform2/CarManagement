const mongoose = require('mongoose');
const mongooseHandler = require('mongoose-handler-nt');
const config = require('config');
const chai = require('chai');
const chaiHTTP = require('chai-http');
const server = require('../app');

const modulesManager = require('../modules');

//const {userModel} = modulesManager.get('document', 'user');


chai.use(chaiHTTP);

/*
async function go()
{
  return new Promise(async (resolve, reject) =>
  {
    it("Connect", async () =>
    {
      await

      await userModel.create(
      {
        firstname: "Jacky",
        name: "Paolo",
        username: "testLogin",
        password: "test",
        role: "admin",
        position: "chef"
      });
    });

    it("Should", async () =>
    {

    });

    it("GO", () =>
    {

  });
}
*/
mongooseHandler.connectDB(config.get("MONGODB"));

const users = require('./users');
users.go();

/*
const sheets = require('./sheets');
sheets.go();

const customers = require('./customers');
customers.go();
*/