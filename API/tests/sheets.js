const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const assert = require('assert');

chai.use(chaiHttp);

let JWT = null;

function go()
{
  describe("Sheets - init tests", () =>
  {
    it("JWT not null", () =>
    {
      assert.notEqual(JWT, null);
    });
  });

  describe("Sheets - Insertion", () =>
  {

  });

  describe("Sheets - Get", () =>
  {

  });

  describe("Sheets - Update", () =>
  {

  });

  describe("Sheets - Delete", () =>
  {

  });
}


function setJWT(jwt)
{
  JWT = jwt;
}

module.exports =
{
  go,
  setJWT
};