const chai = require('chai');
const chaiHTTP = require('chai-http');
const server = require('../app');
const config = require('config');


function getJWT(username, password)
{
  return new Promise((resolve, reject) =>
  {
    chai
    .request(server)
    .post("/api/users/login?api_key=" + config.get("AUTH.API_KEY"))
    .send({username: username, password: password})
    .end((err, res) =>
    {
      if (err)
        return reject(err);

      if (res.status === 200)
      {
        return resolve(res.body.token);
      }
      else
        return reject(new Error('not 200'));
    });
  });
}

module.exports =
{
  getJWT
};