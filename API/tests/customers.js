const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const config = require("config");
const assert = require('assert');

let JWT = null;

function go()
{

  describe("Customers - init tests", () =>
  {
    it("JWT not null", () =>
    {
      assert.notEqual(JWT, null);
    });
  });

  describe("Customers - Insertion", () =>
  {

  });

  describe("Customers - Get", () =>
  {

  });

  describe("Customers - Update", () =>
  {

  });

  describe("Customers - Delete", () =>
  {

  });
}

function setJWT(jwt)
{
  JWT = jwt;
}

module.exports =
{
  go,
  setJWT
};