const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const config = require("config");
const assert = require('assert');
const modulesManager = require('../modules');
const crypto = require('crypto');

const {User} = modulesManager.get('document', 'user');

const utils = require("./utils");

const should = chai.should();

chai.use(chaiHttp);

let JWT = null;


function go()
{
  describe("Users - init tests", () =>
  {
    it("Remove users", async () =>
    {
      await User.remove();
    });

    it("Direct insertion of an user", async () =>
    {

      const password = "test";
      const salt = crypto.randomBytes(16).toString('hex');
      const hashed_password = crypto.createHmac('sha512', password+salt).digest('hex');

      try
      {
        await User.create(
        {
          firstname: "Jacky",
          name: "Paolo",
          username: "testLogin",
          password: hashed_password,
          salt: salt,
          role: "admin",
          position: "chef"
        });
      }
      catch (e)
      {
        console.error(e);
        process.exit(1);
      }
    });

    it("Get JWT", async () =>
    {
      try
      {
        JWT = await utils.getJWT("testLogin", "test");
        assert.notEqual(JWT, null);
      }
      catch (e)
      {
        process.exit(1);
      }
    });

  });


  describe("Users - Insertion", () =>
  {
    const User =
    {
      name: "Bredouille",
      firstname: "Jack",
      username: "test",
      password: "test",
      role: "user",
      position: "worker"
    };

    const Admin =
    {
      name: "Dupont",
      firstname: "Jean",
      username: "test2",
      password: "test",
      role: "admin",
      position: "worker"
    };


    it('Should fails : no token', () =>
    {
      chai
      .request(server)
      .post('/api/users')
      .send()
      .end((err, res) =>
      {
        res.should.have.status(401);
      })
    });

    it('Insert User', () =>
    {
      chai
      .request(server)
      .post('/api/users')
      .set("Authorization", "Bearer "+JWT)
      .send(User)
      .end((err, res) =>
      {
        console.log(res.body);
        res.should.have.status(200);
      })
    });

    it('Insert Admin', () =>
    {
      chai
      .request(server)
      .post('/api/users')
      .set("Authorization", "Bearer "+JWT)
      .send(Admin)
      .end((err, res) =>
      {
        res.should.have.status(200);
      })
    });

  });

  describe("Users - Get", () =>
  {
    it('Get users', () =>
    {
      chai
      .request(server)
      .get('/api/users')
      .set("Authorization", "Bearer "+JWT)
      .end((err, res) =>
      {
        res.should.have.status(200);
        assert.equal(res.body.items, 3)
      })
    });
  });

  describe("Users - Update", () =>
  {

  });

  describe("Users - Delete", () =>
  {

  });

}

function setJWT(jwt)
{
  JWT = jwt;
}

module.exports =
{
  go,
  setJWT
};