const crypto = require('crypto');
const mongoose = require('mongoose');
const {User} = require('../src/models/User');


function insertAdmin()
{
  console.log("Insert admin func");

  User
  .find({username: "admin"})
  .exec()
  .then(data =>
  {
    console.log(data);
    if (data.length === 0)
    {
      const password = "test";
      const salt = crypto.randomBytes(16).toString('hex');
      const hashed_password = crypto.createHmac('sha512', password+salt).digest('hex');

      console.log("Create admin");
      User.create(
      {
        name: "Dupont",
        firstname: "Jean",
        username: "admin",
        password: hashed_password,
        salt: salt,
        role: "admin",
        position: "chef"
      });
    }
  })
  .catch(err =>
  {
    console.log(err);
  });
}

module.exports =
{
  insertAdmin
};