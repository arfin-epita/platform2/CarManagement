const express = require('express');
const router = express.Router();
const modulesManager = require('@modulesManager');

// Lib
const lib = modulesManager.get('module', 'customers', 'lib');

//Handler
const authHandler = require('@authHandler');
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');

module.id = "core/customers/router/index";


router.get('/', (req, res, next) =>
{
  lib
  .getAll(req)
  .then(items =>
  {
    return res.json(items);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});


router.get("/:id", (req, res, next) =>
{
  lib
  .getOne(req, req.params.id)
  .then(item =>
  {
    return res.json(item);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});

router.post("/", (req, res, next) =>
{
  lib
  .createOne(req, req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});


router.put("/:id", (req, res, next) =>
{
  lib
  .updateOne(req, req.params.id, req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});

router.delete("/:id", (req, res, next) =>
{
  lib
  .deleteOne(req, req.params.id)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});

module.exports = router;