const mongoose = require('mongoose');

// Handlers
const logHandler = require('@logHandler');
const errorHandler = require('@errorHandler');
const modulesManager = require('@modulesManager');

// Model
const {Customer} = modulesManager.get('document', 'customer');

module.id = "core/customer/dataAccess/index";



function findAll(req, queryObj = {}, selectObj = {})
{
   return new Promise((resolve, reject) =>
   {
     Customer
     .find(queryObj, selectObj)
     .exec()
     .then(data =>
     {
       return resolve(data);
     })
     .catch(err =>
     {
       return reject(err);
     });

   });
}

function findByID(req, id)
{
  return new Promise((resolve, reject) =>
  {
    Customer
    .findById(id)
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}

function insert(req, obj)
{
  return new Promise((resolve, reject) =>
  {
      Customer
      .create(obj)
      .exec()
      .then(data =>
      {
        return resolve(data);
      })
      .catch(err =>
      {
        return reject(err);
      });
  });
}


function updateByID(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    Customer
    .findByIdAndUpdate(id, obj, {new: true})
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}

function deleteByID(req, id)
{
  return new Promise((resolve, reject) =>
  {
    Customer
    .findByIdAndDelete(id)
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    })
  });
}



module.exports =
{
  findAll,
  findByID,
  insert,
  updateByID,
  deleteByID
};


