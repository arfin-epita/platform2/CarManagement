const config = require('config');

// Handlers
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');

const modulesManager = require('@modulesManager');

// DA
const dataAccess = modulesManager.get('module', 'customers', 'dataAccess');

module.id = "core/customers/lib/index";



function getAll(req)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve([]);

    dataAccess
    .findAll(req)
    .then(data =>
    {
      return resolve({items: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    });
  });
}


function getOne(req, id)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve({});

    dataAccess
    .findByID(req, id)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    })
  });
}



function createOne(req, obj)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);

    dataAccess
    .insert(req, obj)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    });
  });
}

function updateOne(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);


    dataAccess
    .updateByID(req, id, obj)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    })
  });
}

function deleteOne(req, id)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);

    dataAccess
    .deleteByID(req, id)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    });
  });
}

module.exports =
{
  getAll,
  getOne,
  createOne,
  updateOne,
  deleteOne
};