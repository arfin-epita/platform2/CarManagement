const config = require('config');

// Handlers
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');

const modulesManager = require('@modulesManager');

// DA
const dataAccess = modulesManager.get('module', 'sheets', 'dataAccess');

module.id = "core/sheets/lib/index";



function getAll(req)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve([]);

    dataAccess
    .findAll(req)
    .then(data =>
    {
      return resolve({items: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    });
  });
}


function getOne(req, id)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve({});

    dataAccess
    .findByID(req, id)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    })
  });
}



function createOne(req, obj)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);

    dataAccess
    .insert(req, obj)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    });
  });
}

function updateOne(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);


    dataAccess
    .updateByID(req, id, obj)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    })
  });
}

function deleteOne(req, id)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);

    dataAccess
    .deleteByID(req, id)
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logHandler.track(module, arguments)));
    });
  });
}

function createActivity(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);

    dataAccess
    .insertActivity(req, id, obj)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(err);
    })
  });
}

function deleteActivity(req, id, activity_id)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve(null);

    dataAccess
    .deleteActivity(req, id, activity_id)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(err);
    })
  });
}


module.exports =
{
  getAll,
  getOne,
  createActivity,
  createOne,
  updateOne,
  deleteOne,
  deleteActivity
};