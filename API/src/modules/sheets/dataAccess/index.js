const mongoose = require('mongoose');

// Handlers
const logHandler = require('@logHandler');
const errorHandler = require('@errorHandler');
const modulesManager = require('@modulesManager');

// Model
const {Sheet} = modulesManager.get('document', 'sheet');

module.id = "core/sheets/dataAccess/index";



function findAll(req, queryObj = {}, selectObj = {})
{
  return new Promise((resolve, reject) =>
  {
    Sheet
    .find(queryObj, selectObj)
    .populate("car_id")
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });

  });
}

function findByID(req, id)
{
  return new Promise((resolve, reject) =>
  {
    Sheet
    .findById(id)
    .populate("car_id")
    .populate("user_id")
    .exec()
    .then(data =>
    {
      console.log(data);
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}

function insert(req, obj)
{
  return new Promise((resolve, reject) =>
  {
    Sheet
    .create(obj)
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}


function updateByID(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    Sheet
    .findByIdAndUpdate(id, obj, {new: true})
    .populate("car_id")
    .populate("user_id")
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}

function deleteByID(req, id)
{
  return new Promise((resolve, reject) =>
  {
    Sheet
    .findByIdAndDelete(id)
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    })
  });
}

function insertActivity(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    Sheet
    .findById(id)
    .then(data =>
    {
      data.work.activities.push(obj);
      data.save(async (err) =>
      {
        if (err)
          return reject(err);

        const d = await findByID(req, id);
        return resolve(d);
      });


    });
  });
}


function deleteActivity(req, id, activity_id)
{
  return new Promise((resolve, reject) =>
  {
    Sheet
    .findById(id)
    .populate("car_id")
    .populate("user_id")
    .exec()
    .then(data =>
    {
      //logHandler.log("verbose", activity_id);
      data.work.activities = data.work.activities.filter(el => el._id.toString() !== activity_id.toString());
      //logHandler.log("verbose", data.work.activities);
      data.save((err, el) =>
      {
        if (err)
          return reject(err);

        return resolve(el);
      });
    })
    .catch(err =>
    {
      return reject(err);
    })
  });
}


module.exports =
{
  findAll,
  findByID,
  insert,
  updateByID,
  deleteByID,
  insertActivity,
  deleteActivity
};


