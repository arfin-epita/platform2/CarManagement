const express = require('express');
const router = express.Router();
const modulesManager = require('@modulesManager');

// Lib
const lib = modulesManager.get('module', 'cars', 'lib');

//Handler
const authHandler = require('@authHandler');
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');

module.id = "core/cars/router/index";


router.get('/', authHandler.checkJWT, (req, res, next) =>
{
  lib
  .getAll(req)
  .then(items =>
  {
    return res.json(items);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});


router.get("/:id", authHandler.checkJWT, (req, res, next) =>
{
  lib
  .getOne(req, req.params.id)
  .then(item =>
  {
    return res.json(item);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});

router.post("/", authHandler.checkJWT, (req, res, next) =>
{
  logHandler.log('verbose', req.body);
  lib
  .createOne(req, req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});


router.put("/:id", authHandler.checkJWT, (req, res, next) =>
{
  lib
  .updateOne(req, req.params.id, req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});

router.delete("/:id", authHandler.checkJWT, (req, res, next) =>
{
  lib
  .deleteOne(req, req.params.id)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});

module.exports = router;