const mongoose = require('mongoose');

// Handlers
const logHandler = require('@logHandler');
const errorHandler = require('@errorHandler');
const modulesManager = require('@modulesManager');

// Model
const {Car} = modulesManager.get('document', 'car');

module.id = "core/cars/dataAccess/index";



function findAll(req, queryObj = {}, selectObj = {})
{
   return new Promise((resolve, reject) =>
   {
     Car
     .find(queryObj, selectObj)
     .exec()
     .then(data =>
     {
       return resolve(data);
     })
     .catch(err =>
     {
       return reject(err);
     });

   });
}

function findByID(req, id)
{
  return new Promise((resolve, reject) =>
  {
    Car
    .findById(id)
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}

function insert(req, obj)
{
  return new Promise((resolve, reject) =>
  {
      Car
      .create(obj)
      .then(data =>
      {
        return resolve(data);
      })
      .catch(err =>
      {
        return reject(err);
      });
  });
}


function updateByID(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    Car
    .findByIdAndUpdate(id, obj, {new: true})
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}

function deleteByID(req, id)
{
  return new Promise((resolve, reject) =>
  {
    Car
    .findByIdAndDelete(id)
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    })
  });
}



module.exports =
{
  findAll,
  findByID,
  insert,
  updateByID,
  deleteByID
};


