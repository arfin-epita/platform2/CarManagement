const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const config = require('config');

// Handlers
const errorHandler = require('@errorHandler');
const logger = require('@logHandler');

const modulesManager = require('@modulesManager');

// DA
const dataAccess = modulesManager.get('module', 'users', 'dataAccess');

module.id = "core/users/lib/index";



function getAll(req)
{
  return new Promise((resolve, reject) =>
  {
    if (!dataAccess)
      return resolve([]);

    dataAccess.findAll(req)
    .then(data =>
    {
      return resolve({items: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logger.track(module, arguments)));
    });
  });
}


function getOne(req, id)
{
  return new Promise((resolve, reject) =>
  {
    dataAccess.findByID(req, id)
    .then(data =>
    {
      return resolve({item: data, isMine: (req.user.id === id)});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logger.track(module, arguments)));
    })
  });
}

function initUser(req)
{
  return new Promise((resolve, reject) =>
  {
    createOne(req,
    {
      name: "Dupont",
      firstname: "Jean",
      username: "admin",
      password: "test",
      role: "admin",
      position: "chef"
    })
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    })
  })
}

function createOne(req, obj)
{
  return new Promise(async (resolve, reject) =>
  {

    if (obj.hasOwnProperty("username"))
    {
      try
      {

        const getUsername = await dataAccess.findByUsername(req, obj.username);
        logger.log('verbose', obj);
        if (getUsername !== null)
          return reject(errorHandler.create(400, "Username already exists", logger.track(module, arguments)));

        const salt = crypto.randomBytes(16).toString('hex');
        const hashed_password = crypto.createHmac('sha512', obj.password+salt).digest('hex');

        obj['salt'] = salt;
        obj['password'] = hashed_password;
        obj['role'] = "admin";


        dataAccess
        .insert(req, obj)
        .then(data =>
        {
          delete data.salt;
          delete data.password;

          return resolve(data);
        })
        .catch(err =>
        {
          return reject(errorHandler.cast(err));
        });
      }
      catch(err)
      {
        return reject(errorHandler.cast(err));
      }
    }
  });
}

function updateOne(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    dataAccess.update(req, id, obj)
    .then(data =>
    {
      return resolve({item: data});
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logger.track(module, arguments)));
    });
  });
}

function deleteOne(req, id)
{
  return new Promise((resolve, reject) =>
  {
    dataAccess.deleteByID(req, id)
    .then(data =>
    {
      if (!data)
        return reject(errorHandler.create(404, "User not found", logger.track(module, arguments)));

      return resolve(data);
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err, logger.track(module, arguments)));
    })
  });
}

function login(req, obj)
{
  return new Promise((resolve, reject) =>
  {
    if (obj.hasOwnProperty("username") && obj.hasOwnProperty("password"))
    {
      if (obj.username !== "" || obj.password !== "")
      {
        dataAccess.findByUsername(req, obj.username, true)
        .then(data =>
        {
          if (data && checkHashedPassword(data, obj.password))
          {
            const token = createJWTToken(data);

            data.salt = undefined;
            data.password = undefined;

            return resolve({token: token, user: data});
          }

          return reject(errorHandler.create(401, {type: 'LOGIN_NOT_VALID', message: ""}, logger.track(module, arguments)));
        })
        .catch(err =>
        {
          return reject(errorHandler.cast(err, logger.track(module, arguments)));
        });
      }
      else
        return reject(errorHandler.create(400, "Empty field", logger.track(module, arguments)));
    }
    else
      return reject(errorHandler.create(400, "Missing field", logger.track(module, arguments)));

  });
}



// Returns bool
function checkHashedPassword(user, password)
{
  const hashed_password = crypto.createHmac('sha512', password+user.salt).digest('hex');

  return (user.password === hashed_password);
}

// Returns token (string)
function createJWTToken(user)
{
  return jwt.sign(
  {
    exp: Math.floor(Date.now() / 1000) + (60*60),
    data: {id: user._id, role: user.role}
  }, config.get("AUTH.JWT_TOKEN"));

}


module.exports =
{
  getAll,
  getOne,
  createOne,
  updateOne,
  deleteOne,
  login,
  initUser
};