const express = require('express');
const router = express.Router();
const modulesManager = require('@modulesManager');

// Lib
const lib = modulesManager.get('module', 'users', 'lib');

//Handler
const authHandler = require('@authHandler');
const errorHandler = require('@errorHandler');
const logHandler = require('@logHandler');

module.id = "core/users/router/index";

router.get('/ping', (req, res, next) =>
{
  return res.json({message: "Hello world !"});
});

router.get('/', authHandler.checkJWT, (req, res, next) =>
{
  lib.getAll(req)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});

router.get('/init', (req, res, next) =>
{
  lib
  .initUser(req)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});

router.get('/:id', authHandler.checkJWT, (req, res, next) =>
{
  lib.getOne(req, req.params.id)
  .then(item =>
  {
    return res.json(item);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});


router.post('/', authHandler.checkJWT, (req, res, next) =>
{
  lib
  .createOne(req, req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});

/* POST login form. */
router.post('/login', authHandler.checkAPIKey, (req, res, next) =>
{
  logHandler.log('verbose', req.body);
  lib
  .login(req, req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  });
});


router.put('/:id', authHandler.checkJWT, (req, res, next) =>
{
  lib.updateOne(req, req.params.id, req.body)
  .then(data =>
  {
    return res.json(data);
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});

router.delete('/:id', authHandler.checkJWT, (req, res, next) =>
{
  lib.deleteOne(req, req.params.id)
  .then(data =>
  {
    logHandler.log('verbose', data);
    return res.json({status: 1});
  })
  .catch(err =>
  {
    return errorHandler.send(res, err);
  })
});


module.exports = router;
