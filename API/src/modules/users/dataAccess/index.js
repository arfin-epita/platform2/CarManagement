const mongoose = require('mongoose');

// Handlers
const logHandler = require('@logHandler');
const errorHandler = require('@errorHandler');
const modulesManager = require('@modulesManager');

// Model
const {User} = modulesManager.get('document', 'user');

module.id = "core/users/dataAccess/index";



function findAll(req)
{
  return new Promise((resolve, reject) =>
  {
    User
    .find({})
    .select('-salt -password')
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}


function findByID(req, id, showCredentials = false)
{
  return new Promise((resolve, reject) =>
  {
    const q = (showCredentials) ?
            User.findById(id) :
            User.findById(id).select('-salt -password');
    q
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}


function findByUsername(req, username, showCredentials = false)
{
  return new Promise((resolve, reject) =>
  {
    const q = (showCredentials) ?
            User.findOne({username: username}) :
            User.findOne({username: username}).select('-salt -password');
    q
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}


function insert(req, obj)
{
  return new Promise((resolve, reject) =>
  {

    User
    .create(obj, (err, data) =>
    {
      if (err)
        return reject(err);

      return resolve(data);
    })
  });
}


function update(req, id, obj)
{
  return new Promise((resolve, reject) =>
  {
    User
    .findByIdAndUpdate(id, obj, {new: true})
    .select('-salt -password')
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}


function deleteByID(req, id)
{
  return new Promise((resolve, reject) =>
  {
    User
    .findByIdAndRemove(id)
    .exec()
    .then(result =>
    {

      return resolve(result);
    })
    .catch(err =>
    {
      return reject(err);
    });
  });
}


module.exports =
{
  findAll,
  findByID,
  findByUsername,
  insert,
  update,
  deleteByID
};
