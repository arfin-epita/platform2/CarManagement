const mongoose = require('mongoose');

const {activitySchema} = require('./Activity');

const schema = new mongoose.Schema(
{
  title: {type: String, default: null},
  car_id: {type: mongoose.Schema.ObjectId, ref: "Car", default: null},
  user_id: {type: mongoose.Schema.ObjectId, ref: "User", default: null},
  customer_id: {type: mongoose.Schema.ObjectId, ref: "Customer", default: null},
  car_mileage: {type: Number, default: null},
  date_begin: {type: Date, default: null},
  date_end: {type: Date, default: null},
  work:
  {
    duration: {type: Number, default: null},
    activities: {type: [activitySchema], default: []}
  }
},
{
  collection : 'sheets',
  strict: true
});

const Sheet = mongoose.model('Sheet', schema);

module.exports = {Sheet};