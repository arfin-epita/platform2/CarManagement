const mongoose = require('mongoose');

const activitySchema = new mongoose.Schema(
{
  title: {type: String, default: null},
  description: {type: String, default: null},
  price: {type: Number, default: 0},
  VAT: {type: Number, default: 20},
  state: {type: String, enum: ['init', 'started', 'finished'], default: "init"},
  user_id : {type: mongoose.Schema.ObjectId, default: null},
  validation:
  {
    required: {type: Boolean, default: false},
    date: {type: Date, default: null},
    user_id: {type: mongoose.Schema.ObjectId, default: null}
  }
},
{
  strict: true
});

module.exports = {activitySchema};