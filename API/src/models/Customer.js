const mongoose = require('mongoose');


const schema = new mongoose.Schema(
{
  name: {type: String, default: null},
  firstname: {type: String, default: null},
  relationshipSince: {type: Date, default: Date.now()}
},
{
  collection : 'customers',
  strict: true
});

const Customer = mongoose.model('Customer', schema);

module.exports = {Customer};