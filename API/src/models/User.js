const mongoose = require('mongoose');


const schema = new mongoose.Schema(
{
  name: {type: String, default: null},
  firstname: {type: String, default: null},
  username: {type: String, required: true},
  password: {type: String, required: true},
  salt : {type: String, default: null},
  role: {type: String, default: "user", enum: ["admin", "user", "guest"]},
  position: {type: String, default: null}
},
{
  collection : 'users',
  strict: true
});

//schema.plugin(metaManager.metaManager);

const User= mongoose.model('User', schema);

module.exports = {User};