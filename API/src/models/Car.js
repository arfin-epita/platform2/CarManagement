const mongoose = require('mongoose');

const carSchema = new mongoose.Schema(
{
  model: {type: String, default: null},
  brand: {type: String, default: null},
  year: {type: Number, default: null},
  numberplate: {type: String, default: null}
},
{
  collection: 'cars',
  strict: true
});

const Car = mongoose.model('Car', carSchema);

module.exports = {Car};