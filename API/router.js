const express = require('express');
const router = express.Router();


const homeRouter = require('@modules/home/router/index');
const usersRouter = require('@modules/users/router/index');
const sheetsRouter = require('@modules/sheets/router/index');
const customersRouter = require('@modules/customers/router/index');
const carsRouter = require('@modules/cars/router/index');

router.use("/home", homeRouter);
router.use('/users', usersRouter);
router.use('/sheets', sheetsRouter);
router.use('/customers', customersRouter);
router.use('/cars', carsRouter);

module.exports = router;